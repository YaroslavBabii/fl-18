let prize = 0;

function generateNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min);

}

let firstPrize = 100;
let secondPrize = 50;
let thirdPrize = 25;

let three = 3;
let two = 2;
let four = 4;
let eight = 8;



function calcPrize(attempt, game) {
    if (attempt === 1) {
        return firstPrize * game;
    } else if (attempt === two) {
        return secondPrize * game;
    } else if (attempt === three) {
        return thirdPrize * game;
    }
    return 0;
}

function startGame(min, max, gameCount) {
    let attempts = three;
    let win = false;
    let randomNumber = generateNumber(min, max);
    for (let i = 0; i < attempts; i++) {
        let possiblePrize = calcPrize(i + 1, gameCount);
        let userNumber = parseInt(prompt(`Choose a roulette number from ${min} to ${max}: 
        Attempts left: ${attempts - i}
        Total prize: ${prize}
        Possible prize on current attempts: ${possiblePrize}`));
        if (userNumber === randomNumber) {
            prize += possiblePrize;
            win = true;
            break;
        }
    }
    if (win) {
        alert(`Congratulation, you won! Your prize is: ${prize}$.`);
    } else {
        alert(`Thank you for your participation. Your prize is: ${prize}$.`)
    }
    if (confirm('Do you want to continue?')) {
        startGame(min, max + four, gameCount + 1);
    } else {
        alert('You did not become a billionaire, but can.');
    }
}

function askToPlay() {
    if (confirm('Do you want to play a game?')) {
        startGame(0, eight, 1);

    } else {
        alert('You did not become a billionaire, but can.');
    }
}

askToPlay();