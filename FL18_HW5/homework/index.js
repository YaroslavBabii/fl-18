// Task #1
function isEquals(x = 3, y = 3) {
    let result = x === y;
    return result

}


// Task #2
function isBigger(x = 5, y = -1) {
    let result = x > y;
    return result;

}


// Task #3
function storeNames(...theArgs) {
    const arr = [...theArgs]
    return arr

}



// Task #4
function getDifferent(x, y) {
    if (x > y) {
        let c = x - y
        return c
    } else if (x < y) {
        let c = y - x
        return c
    }

}

// Task #5
function negativeCount(arr) {
    return arr.filter(value => value < 0).length;
}



// Task #6
function letterCount(a, b) {
    return a.split('').filter(z => z === b).length

}



// Task #7
function countPoints(array) {

    return array
        .map((el) => el.split(':'))
        .reduce((acc, el) => {
            if (+el[0] > +el[1]) {
                return acc + 3;
            } else if (+el[0] === +el[1]) {
                return ++acc;
            } else {
                return acc;
            }
        }, 0)

}